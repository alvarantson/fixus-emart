from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
def index(request):
	try:
		if request.session["kasutaja"] not in locals():
			request.session["kasutaja"] = "-"
	except:
		request.session["kasutaja"] = "-"
	mingi_list1 = ["Sepik", "5", "http://www.eki.ee/dict/psv/__pildid/sepik.jpg"]
	mingi_list2 = ["Leib", "2", "https://europagar.ee/wp-content/uploads/2016/05/Must-leib.jpg"]
	mingi_list3 = ["Saib", "3", "https://m1.selver.ee/media/catalog/product/cache/1/image/409x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740072123653.jpg"]
	kokku = [mingi_list1, mingi_list2, mingi_list3]
	if bool(request.POST) == True:
		if request.POST["submit-button"] == "form1":
			print(request.POST["Nimi"])
		if request.POST["submit-button"] == "pass_form":
			if request.POST["parool1"] == request.POST["parool2"]:
				request.session["kasutaja"] = request.POST["kasutaja"]
	return render(request, "index.html", context={"Tooted": kokku})
