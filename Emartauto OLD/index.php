<!DOCTYPE html>
<html lang="en">


<head>
  <meta charset="utf-8">
  <title>OÜ Emart auto </title>
  <meta name="OÜ Emart auto" content="OÜ Emart auto kodulehekülg">
  <meta name="propeller" content="a5cf8176c60ad32f0bf7087b3a453a3a" />
    <script src="static/bootstrap/jquery-1.12.4.js"></script>
    <script src="static/bootstrap/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="static/bootstrap/font.css">
    <link rel="stylesheet" type="text/css" href="static/bootstrap/bootstrap.min.css">
  <script type="text/javascript" src="//go.onclasrv.com/apu.php?zoneid=874799"></script>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'  type='text/css'>
  <link rel="stylesheet" type="text/css" href="static/index2.css">
  
  
</head>
<body style="font-family: 'Roboto', sans-serif;" background="static/bg.jpg">
<nav class="navbar navbar-fixed-top" style="border-radius:0px; margin-bottom: 0; float: none; border-bottom-width;background: linear-gradient(#25368F, #4052b3)">
  <div class="container-fluid" style=" height: 50px border-bottom: 5px ;border-color: #2a17b4; opacity: 1;">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
        <span class="icon-bar" style="background-color: white"></span>
        <span class="icon-bar" style="background-color: white"></span>
        <span class="icon-bar" style="background-color: white"></span>
      </button>
    </div>
  </div>
  <div class="collapse navbar-collapse" id="mainNavBar" style="align-items: center;">
    <ul class="nav navbar-nav" style="align-items: center; color: white">
      <li><a href="index.php" style="padding: 0 0 0 10px"><img src="static/Emart_Auto-logo.png" style="height: 70px; "></a></li>
      <li><a href="index.php"><h4 style="color:white"> Avaleht </h4></a></li>
      <li><a href="auto.html"><h4 style="color:white"> Autotarbed </h4></a></li>
            <li><a href="remont.html"><h4 style="color:white"> Töökoda </h4></a></li>
            <li><a href="minimarket.html"><h4 style="color:white"> Toidukaubad </h4></a></li>
            <li><a href="valkla.html"><h4 style="color:white"> Valkla Kauplus </h4></a></li>
            <li><a href="meist.html"><h4 style="color:white"> Meist </h4></a></li>
    </ul>
  </div>
</nav>
<div class="col-xs-12" style="width: 100%; height: 95vh">
    <div class="hovereffect">
        
            <div class="overlay">
                <h2><img class="img-responsive" src="static/Emart_Auto-logo.png" style="width: 75%; height: 75%; left: 16%;margin: 50% 0 30% 0"></h2>
                <div class="col-xs-12" id="sad"> 
                    <div >
                      <div class="col-xs-12">
                        <div class="col-xs-1" class="esipilt" id="esipilt1">
                          <a href="auto.html">
                              <img src="static/auto_esi.png" >
                          </a>
                      </div>
                        <div class="col-xs-1" class="esipilt" id="esipilt2">
                          <a href="remont.html">
                              <img src="static/remont_esi.png" >
                          </a>
                      </div>
                        <div class="col-xs-1" class="esipilt" id="esipilt3">
                          <a href="minimarket.html">
                              <img src="static/minimarket_esi.png" >
                          </a>
                      </div>
                        <div class="col-xs-1" class="esipilt" id="esipilt4">
                          <a href="valkla.html">
                              <img src="static/valkla_esi.png" >
                          </a>
                      </div>
                      </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<div>
  <div class="col-xs-12" style="background: #fff; width: 100%; height: 100%; padding-bottom: 2.5%">
    <div class="col-xs-12" style="background: #fff">
      <h1 style="padding:3.5% 0 2.5% 0;text-align: center">Emart Autokaubad</h1>
    </div>
    <div class="col-xs-12 col-md-6" style="background: #fff; padding: 5% 0 0 5%; ">
      <p>OÜ Emart Auto alustas tegevust 1996. aastal auto varuosade jaemüügiga Kuusalu alevikus, Harjumaal.
      <br><br>
      Autokaupade pood müüb erinevaid autotarvikuid, autorehve, jalgrattaid ja varuosi, tööriistu ning rakiseid. Autokaupade pood asub kahe teise poe (Emart Minimarketi ning Emart Töökoja) vahel.
      </p>
      <a href="auto.html">
        <h3>Uuri lisaks!</h3>
      </a>
    </div>
    <div class="col-xs-12 col-md-2">
      <h3 style="padding-top: 25%; text-align: center">
        E-R 9.00-18.30 <br>
        L    10.00- 17.00 <br><br>
        TEL.6072349<br>
        <span style="font-size: 70%">kuusalu@emartauto.ee</span> <br> <br>
        Kuusalu tee 51, Kuusalu, 74601 Harju maakond
      </h3>
    </div>
    <div class="col-xs-12 col-md-4" style="background: #fff; text-align: center; padding: 0 0 0 0; ">
      <img src="static/tookoda.png" style="width: 75%; margin-top: 15%">
    </div>
    <hr style="margin: 0 0 0 0; padding: 0 0 0 0">
  </div>
  <div class="col-xs-12" style="background: #f8f8fa; width: 100%; height: 100%; padding-bottom: 2.5%">
    <div class="col-xs-12" style="background: #f8f8fa">
      <h1 style="padding:3.5% 0 2.5% 0;text-align: center">Emart Töökoda</h1>
    </div>
    <div class="col-xs-12 col-md-6" style="background: #f8f8fa; padding: 5% 0 0 5%; ">
      <p>1996 mõni kuu, peale varuosade kaupluse avamist asutati varuosade kaupluse juurde ka autohoolduse töökoda.
      <br><br>
            Töökoja tööd jagunevad nelja kategooriasse: aiatöömasinate ja seadmete remont, jalgrataste remont, rehvitööd, autoremondi- ja hooldustööd, diagnostika ja elektritööd. Peamiselt keskendub töökoda autoremondile. Emart Töökoda asub sisehoovist vaadates peamaja vasakus osas.
      </p>
      <a href="remont.html">
        <h3>Uuri lisaks!</h3>
      </a>
    </div>
    <div class="col-xs-12 col-md-2">
      <h3 style="padding-top: 25%; text-align: center">
        E-R 9.00-18.30<br>
        L    10.00- 17.00<br><br>
        TEL.6072349<br>
        <span style="font-size: 70%">kuusalu@emartauto.ee</span> <br> <br>
        Kuusalu tee 51, Kuusalu, 74601 Harju maakond
      </h3>
    </div>
    <div class="col-xs-12 col-md-4" style="background: #f8f8fa; text-align: center; padding: 0 0 0 0; ">
      <img src="static/tookoda.png" style="width: 75%; margin-top: 15%">
    </div>
    <hr style="margin: 0 0 0 0; padding: 0 0 0 0">
  </div>
  <div class="col-xs-12" style="background: #fff; width: 100%; height: 100%; padding-bottom: 2.5%">
    <div class="col-xs-12" style="background: #fff">
      <h1 style="padding:3.5% 0 2.5% 0;text-align: center">Emart Minimarket</h1>
    </div>
    <div class="col-xs-12 col-md-6" style="background: #fff; padding: 5% 0 0 5%; ">
      <p>1997. aastal lisandus tegevusvaldkonda toidukaubandus ja kiirtoitlustus.
      <br><br>
      Minimarket müüb lisaks alkoholile, jäätisele, tubakale ning snäkkidele ka veel kiirtoitu. Näiteks saab tellida klient omale burgeri, mis valmib vaid mõne minutiga. Samuti saab osta koha pealt sooja kohvi! Emart Minimarket asub sisehoovist vaadates hoone paremale jäävas osas. Toidupoe kõrvale jääb terass.
      </p>
      <a href="minimarket.html">
        <h3>Uuri lisaks!</h3>
      </a>
    </div>
    <div class="col-xs-12 col-md-2">
      <h3 style="padding-top: 25%; text-align: center">
      
        E-R 9.00- 21.00<br>
        L    10.00-21.00<br>
        P    10.00-18.00<br><br>
        TEL6097715<br>
        <span style="font-size: 90%">info@emartauto.ee</span> <br> <br>
        Kuusalu tee 51, Kuusalu, 74601 Harju maakond
      </h3>
    </div>
    <div class="col-xs-12 col-md-4" style="background: #fff; text-align: center; padding: 0 0 0 0; ">
      <img src="static/tookoda.png" style="width: 75%; margin-top: 15%">
    </div>
    <hr style="margin: 0 0 0 0; padding: 0 0 0 0">
  </div>
  <div class="col-xs-12" style="background: #f8f8fa; width: 100%; height: 100%; padding-bottom: 2.5%">
    <div class="col-xs-12" style="background: #f8f8fa">
      <h1 style="padding:3.5% 0 2.5% 0;text-align: center">Valkla Toidukauplus</h1>
    </div>
    <div class="col-xs-12 col-md-6" style="background: #f8f8fa; padding: 5% 0 0 5%; ">
      <p>1998. aastal asutasime toidukaupluse Valkla külas. Kauplus on mõeldud minimarketi või toidupoena kõikidele kohalikele. 
      <br><br>
      Pakume Omniva postipunkti teenust. Kaupluses müüakse alkoholi, suitsu, kergemaid suupisteid, alkoholivabu jooke, piimatooteid, snäkke, jne... Valkla Toidukauplus asub Valkla külas, Kuusalu lähedal, Harju maakonnas.
      </p>
      <a href="valkla.html">
        <h3>Uuri lisaks!</h3>
      </a>
    </div>
    <div class="col-xs-12 col-md-2">
      <h3 style="padding-top: 25%; text-align: center">
        T-L 10.00-18.00<br><br>
        TEL.5206929<br>
        <span style="font-size: 90%">info@emartauto.ee</span> <br> <br>
        Valkla küla, Kuusalu vald, Harjumaa, 74630
      </h3>
    </div>
    <div class="col-xs-12 col-md-4" style="background: #f8f8fa; text-align: center; padding: 0 0 0 0; ">
      <img src="static/valkla.png" style="width: 75%; margin-top: 15%">
    </div>
    <hr style="margin: 0 0 0 0; padding: 0 0 0 0">
  </div>
  
</div>
</body>  
</html>