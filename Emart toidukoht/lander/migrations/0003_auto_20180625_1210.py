# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-25 09:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lander', '0002_auto_20180625_1155'),
    ]

    operations = [
        migrations.CreateModel(
            name='landerlang',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lang', models.CharField(max_length=999, unique=True)),
                ('nimi', models.CharField(max_length=99999)),
                ('lahtiolek', models.TextField(max_length=99999)),
                ('restost', models.CharField(max_length=99999)),
                ('restost2', models.TextField(max_length=99999)),
                ('restostpilt', models.ImageField(upload_to='')),
                ('menyyst', models.CharField(max_length=99999)),
                ('menyyst2', models.TextField(max_length=99999)),
                ('menyystpic1', models.ImageField(upload_to='')),
                ('menyystpic2', models.ImageField(upload_to='')),
                ('menyystpic3', models.ImageField(upload_to='')),
                ('menyystpic4', models.ImageField(upload_to='')),
                ('menyyst3', models.CharField(max_length=99999)),
            ],
        ),
        migrations.DeleteModel(
            name='lang',
        ),
    ]
