from django.db import models

# Create your models here.
class landerlang(models.Model):
	lang = models.CharField(max_length=999, unique=True)
	nimi = models.CharField(max_length=99999)
	lahtiolek = models.TextField(max_length=99999)
	restost = models.CharField(max_length=99999)
	restost2 = models.TextField(max_length=99999)
	restostpilt = models.ImageField()

	menyyst = models.CharField(max_length=99999)
	menyyst2 = models.TextField(max_length=99999)
	menyystpic1 = models.ImageField()
	menyystpic2 = models.ImageField()
	menyystpic3 = models.ImageField()
	menyystpic4 = models.ImageField()
	menyyst3 = models.CharField(max_length=99999)

#	yhendust = models.CharField(max_length=99999)
#	yhendust2 = models.TextField(max_length=99999)
	def __str__(self):
		return self.lang