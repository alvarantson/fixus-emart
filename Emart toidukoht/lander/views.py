from django.shortcuts import render
from index.models import keel
from about.models import contact
from .models import landerlang
from navbar.models import navlang
from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.
def lander(request):
	if 'lang' not in request.session:
		request.session['lang'] = 'est'
	keeled = []
	for item in keel.objects.all():
		keeled.append(item)
	keel2 = keel.objects.get(nimi=request.session['lang'])
	keel3 = landerlang.objects.get(lang=request.session['lang'])
	keelnav = navlang.objects.get(lang=request.session['lang'])
	contactvar = contact.objects.all()[0]
	if bool(request.POST) == True: 
		if request.POST['submit-btn'] == 'lang':
			request.session['lang'] = request.POST['langselect']
			keel2 = keel.objects.get(nimi=request.session['lang'])
			return HttpResponseRedirect('/')
	return render(request, 'lander.html', context={'langs':keeled, 'lang': keel2, 'lang2': keel3, 'contact': contactvar, 'navlang':keelnav})