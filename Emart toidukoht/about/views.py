from django.shortcuts import render
from index.models import keel
from navbar.models import navlang
from .models import contact, aboutlang, contactform
from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.
def about(request):
	if 'lang' not in request.session:
		request.session['lang'] = 'est'
	keeled = []
	for item in keel.objects.all():
		keeled.append(item)
	keel2 = keel.objects.get(nimi=request.session['lang'])
	keel3 = aboutlang.objects.get(lang=request.session['lang'])
	keelnav = navlang.objects.get(lang=request.session['lang'])
	contactvar = contact.objects.all()[0]
	if bool(request.POST) == True: 
		if request.POST['submit-btn'] == 'lang':
			request.session['lang'] = request.POST['langselect']
			keel2 = keel.objects.get(nimi=request.session['lang'])
			return HttpResponseRedirect('/about')
		if request.POST['submit-btn'] == 'contactform':
			new = contactform.objects.create(
				nimi=request.POST['name'],
				tel_nr=request.POST['tel'],
				e_mail=request.POST['mail'],
				letter=request.POST['letter']
				)
	return render(request, 'about.html', context={'langs':keeled, 'lang': keel2, 'contact': contactvar, 'aboutlang': keel3, 'navlang':keelnav})