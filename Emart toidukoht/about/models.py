from django.db import models

# Create your models here.
class contact(models.Model):
	phone = models.CharField(max_length=99999)
	mail = models.CharField(max_length=99999)
	address = models.CharField(max_length=99999)
	gmaps_x = models.CharField(max_length=99999, default='59.448032')
	gmaps_y = models.CharField(max_length=99999, default='25.432306')
	gmaps_zoom = models.CharField(max_length=2, default='15')
	def __str__(self):
		return 'Ära üle ühe tee!'

class aboutlang(models.Model):
	lang = models.CharField(max_length=99999, unique=True)
	meist = models.CharField(max_length=99999)
	kaardil = models.CharField(max_length=99999)
	kaardil2 = models.CharField(max_length=99999)
	kontakt = models.CharField(max_length=99999)
	kontakt2 = models.CharField(max_length=99999)
	kontakt_nimi = models.CharField(max_length=99999)
	kontakt_tel = models.CharField(max_length=99999)
	kontakt_mail = models.CharField(max_length=99999)
	kontakt_sisu = models.CharField(max_length=99999)
	saada = models.CharField(max_length=99999)
	def __str__(self):
		return self.lang

class contactform(models.Model):
	nimi = models.CharField(max_length=99999)
	tel_nr = models.CharField(max_length=99999)
	e_mail = models.CharField(max_length=99999)
	letter = models.TextField(max_length=99999)
	def __str__(self):
		return self.nimi