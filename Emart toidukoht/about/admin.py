from django.contrib import admin
from .models import contact, aboutlang, contactform
# Register your models here.

admin.site.register(contact)
admin.site.register(aboutlang)
admin.site.register(contactform)