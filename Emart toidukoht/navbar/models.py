from django.db import models

# Create your models here.
class navlang(models.Model):
	lang = models.CharField(max_length=3, unique=True)
	index = models.CharField(max_length=99999)
	order = models.CharField(max_length=99999)
	about = models.CharField(max_length=99999)
	homepage = models.CharField(max_length=99999)
	def __str__(self):
		return self.lang