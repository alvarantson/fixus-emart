from django.shortcuts import render
from .models import toit, tellimus, keel
from about.models import contact
from navbar.models import navlang
from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.
def index(request):
	if 'lang' not in request.session:
		request.session['lang'] = 'est'
	keeled = []
	for item in keel.objects.all():
		keeled.append(item)
	keel2 = keel.objects.get(nimi=request.session['lang'])
	keelnav = navlang.objects.get(lang=request.session['lang'])
	contactvar = contact.objects.all()[0]


	if bool(request.POST) == True: 
		if request.POST['submit-btn'] == 'lang':
			request.session['lang'] = request.POST['langselect']
			keel2 = keel.objects.get(nimi=request.session['lang'])
			return HttpResponseRedirect('/order')

		if request.POST['submit-btn'] == 'toidud':
			toidud = request.POST['toidud'].split(',')[:-1]
			tellimus2 = []
			hind = 0
			for item in toidud:
				tellimus2.append(toit.objects.get(nimi=item))
				hind += toit.objects.get(nimi=item).hind
			request.session['hind'] = hind
			return render(request, 'checkout.html', context={'tellimused':tellimus2, 'hind': hind, 'langs':keeled, 'lang': keel2})
		if request.POST['submit-btn'] == 'tellimus':
			request.session['paid'] = 1
			item_new = tellimus.objects.create(eesnimi=request.POST['eesnimi'],perenimi=request.POST['perenimi'],tel_nr=request.POST['tel_nr'],toit=request.POST['tellimus'], hind=request.session['hind'])
			keel2 = keel.objects.get(nimi=request.session['lang'])
			return render(request, "paid.html", context={'langs':keeled, 'lang': keel2, 'navlang':keelnav})
		if request.POST['submit-btn'] == 'valmis-REMOVE':
			edit = tellimus.objects.get(aeg=request.POST['aeg'], perenimi=request.POST['perenimi'], toit=request.POST['toit'])
			edit.valmis = 1
			edit.save()
		if request.POST['submit-btn'] == 'valmis-OK':
			edit = tellimus.objects.get(aeg=request.POST['aeg'], perenimi=request.POST['perenimi'], toit=request.POST['toit'])
			edit.valmis = 0
			edit.save()
		if request.POST['submit-btn'] == 'jarel-REMOVE':
			edit = tellimus.objects.get(aeg=request.POST['aeg'], perenimi=request.POST['perenimi'], toit=request.POST['toit'])
			edit.jarel = 1
			edit.save()
		if request.POST['submit-btn'] == 'jarel-OK':
			edit = tellimus.objects.get(aeg=request.POST['aeg'], perenimi=request.POST['perenimi'], toit=request.POST['toit'])
			edit.jarel = 0
			edit.save()

	toidud = []
	for item in toit.objects.filter(lang=request.session['lang']):
		item.id = item.nimi.replace(' ','')
		toidud.append(item)
	tellimused = []
	for item in tellimus.objects.all():
		tellimused.append(item)

	return render(request, "index.html", context={'toit':toidud, 'tellimused':reversed(tellimused), 'langs':keeled, 'lang': keel2, 'contact': contactvar, 'navlang':keelnav})