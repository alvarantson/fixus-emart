from django.contrib import admin
from .models import toit, tellimus, keel

# Register your models here.
admin.site.register(toit)
admin.site.register(tellimus)
admin.site.register(keel)