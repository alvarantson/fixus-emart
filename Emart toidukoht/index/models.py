from django.db import models
from datetime import datetime 
# Create your models here.
class toit(models.Model):
	nimi = models.CharField(max_length=999, unique=True)
	lang = models.CharField(max_length=4, default='est')
	kirjeldus = models.TextField(max_length=9999)
	hind = models.IntegerField()
	pilt = models.ImageField()
	def __str__(self):
		return self.nimi+' -'+self.lang+'-'

class tellimus(models.Model):
	eesnimi = models.CharField(max_length=999)
	perenimi = models.CharField(max_length=999)
	tel_nr = models.CharField(max_length=20)
	toit = models.TextField(max_length=9999)
	hind = models.CharField(max_length=20)
	valmis = models.IntegerField("Kas on valmis - 1 on jah, 0 on ei", default=0)
	jarel = models.IntegerField("Kas on järel käidud - 1 on jah, 0 on ei", default=0)
	aeg = models.CharField(default=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),blank=True,editable=False,max_length=999)
	def __str__(self):
		return self.eesnimi+' '+self.perenimi+' '+self.aeg

class keel(models.Model):
	nimi = models.CharField(max_length=4, unique=True)
#	lahtiolek = models.TextField(max_length=99999)
	menyy = models.CharField(max_length=99999, default='Menu')
	kokku = models.CharField(max_length=99999)
	maksa = models.CharField(max_length=99999)
	checkout = models.CharField(max_length=99999)
	eesnimi = models.CharField(max_length=99999)
	perenimi = models.CharField(max_length=99999)
	tel_nr = models.CharField(max_length=99999)
	tanan = models.CharField(max_length=99999)
	avalehele = models.CharField(max_length=99999)
	pilt = models.ImageField(blank=True)
	def __str__(self):
		return self.nimi