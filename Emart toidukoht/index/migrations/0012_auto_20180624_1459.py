# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2018-06-24 11:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0011_auto_20180624_1458'),
    ]

    operations = [
        migrations.AddField(
            model_name='keel',
            name='pilt',
            field=models.ImageField(blank=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='tellimus',
            name='aeg',
            field=models.CharField(blank=True, default='2018-06-24 14:59:12', editable=False, max_length=999),
        ),
    ]
