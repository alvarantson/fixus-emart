# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2018-06-22 06:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='toit',
            name='nimi',
            field=models.CharField(max_length=999, unique=True),
        ),
    ]
