# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-25 09:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0019_auto_20180625_1210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tellimus',
            name='aeg',
            field=models.CharField(blank=True, default='2018-06-25 12:16:16', editable=False, max_length=999),
        ),
    ]
