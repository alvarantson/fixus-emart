
function myMap() {
	var mapProp= {
	    center:new google.maps.LatLng(Number('{{ contact.gmaps_x | linebreaksbr }}'),Number('{{ contact.gmaps_y | linebreaksbr }}')),
	    zoom:Number('{{ contact.gmaps_zoom | linebreaksbr }}'),
	};
	var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	var marker = new google.maps.Marker({
		position: {lat: Number('{{ contact.gmaps_x | linebreaksbr }}'), lng: Number('{{ contact.gmaps_y | linebreaksbr }}')},
		map: map,
		title: 'Emart'
	});
};
