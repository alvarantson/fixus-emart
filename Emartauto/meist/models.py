from django.db import models

# Create your models here.
class meist_lang(models.Model):
	lang = models.CharField(max_length=3, unique=True)
	h1 = models.CharField(max_length=999)
	def __str__(self):
		return self.lang