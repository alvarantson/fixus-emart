from django.db import models

# Create your models here.
class index_lang(models.Model):
	lang = models.CharField(max_length=3, unique=True)
	h1 = models.CharField(max_length=999)
	content = models.CharField(max_length=99999)
	def __str__(self):
		return self.lang
