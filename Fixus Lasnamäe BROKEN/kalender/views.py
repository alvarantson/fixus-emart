from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import kalender_entry
from login.models import worker
# Create your views here.
def kalender(request):
	try:
		request.session['kalender_priority'] = worker.objects.get(name=request.session['worker']).kalender_priority
		request.session['varuosad_priority'] = worker.objects.get(name=request.session['worker']).varuosad_priority
		request.session['tookoda_priority'] = worker.objects.get(name=request.session['worker']).tookoda_priority
	except:
		return HttpResponseRedirect('/login') #INDEX\i puhul '/'


	if bool(request.POST) == True:

		if 'remove' in request.POST['submit-btn']:
			kalender_entry.objects.get(
				nimi=request.POST['submit-btn'].split('-')[1].split('~')[0],
				paev=request.POST['submit-btn'].split('-')[1].split('~')[1]
			).delete()

		if 'new' in request.POST['submit-btn']:
			kalender_entry.objects.create(
				nimi = request.POST['nimi'],
				paev = request.POST['paev'],
				tundide_arv = request.POST['tundide_arv'],
				kes_tegi = request.POST['kes_tegi']
			)

		if 'edit' in request.POST['submit-btn']:
			edit = kalender_entry.objects.get(nimi=request.POST['nimi'], paev=request.POST['paev'])
			edit.nimi = request.POST['nimi']
			edit.paev = request.POST['paev']
			edit.tundide_arv = request.POST['tundide_arv']
			edit.kes_tegi = request.POST['kes_tegi']
			edit.save()


	return render(request, 'kalender.html', context={
		'items':kalender_entry.objects.all().reverse()
		})