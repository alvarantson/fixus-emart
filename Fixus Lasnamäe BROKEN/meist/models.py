from django.db import models

# Create your models here.
class meist_lang(models.Model):
	lang = models.CharField(max_length=3, unique=True)
	h1 = models.CharField(max_length=999, default='')
	msg = models.CharField(max_length=999, default='')
	tekst = models.CharField(max_length=999, default='')
	asukoht = models.CharField(max_length=999, default='')
	pilt = models.ImageField(default='')
	def __str__(self):
		return self.lang